package main

import (
	"bufio"
	"fmt"
	"myapp/doctor"
	"os"
)

func main() {
	// var whatToSay string
	// whatToSay := "Hello, World from variable"
	// sayHelloWorld(whatToSay)

	reader := bufio.NewReader(os.Stdin)

	whatToSay := doctor.Intro()

	fmt.Println(whatToSay)

	userInput, _ := reader.ReadString('\n')

	fmt.Println(userInput)
}

// func sayHelloWorld(whatToSay string) {
// 	fmt.Println(whatToSay)
// }
